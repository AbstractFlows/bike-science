# BikeScience

BikeScience is a open source project that relies on advanced Data Science techniques for investigating Bike-Sharing Systems. 

## Getting started

BikeScience allows urban planners to analyze and understand the impact of Bike-Sharing Systems in their cities. 
Such knowledge can be used to support future planning and improvements regarding bike-sharing. 

Using BikeScience you can investigate any other interesting issues you may want to know about. 
The instructions below explain the basic requirements to get start with the project. 

## Prerequisites

This project is being developed with Python 3.7. 

### Windows

On Windows, prefer to use Anaconda. [This article](https://geoffboeing.com/2014/09/using-geopandas-windows/)
contains information on installing `geopandas` package on Windows through Anaconda and Pip.

### Linux or MacOS

On Linux or Mac, it is needed to install `libspatialindex` library.

Example (Ubuntu/Debian):

```
sudo apt-get install libspatialindex-dev
```

Example (MacOS):

```
brew install spatialindex
```


### Python packages

* seaborn
* matplotlib
* folium
* pandas
* geopandas
* rtree
* pyproj
* geopy
* jupyter (or jupyterlab)
* numpy 
* scipy 
* sklearn 

You can use [pip](https://pypi.org/project/pip/) or [conda](https://conda.io/) to install these python packages.

```bash
conda install folium
```

## Usage

You can use the current notebooks in the **boston-od-flows** folder to see examples of analyses we've already done. 


## License

This project is licensed under the [MPL 2.0 License](https://www.mozilla.org/en-US/MPL/2.0/) - see the LICENSE file for details.
