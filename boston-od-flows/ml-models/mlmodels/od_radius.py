import shapely.wkt
import geopandas as gpd

RADIUS = 500
lat_lon_projection = {'init': 'epsg:4326'}
meters_projection = {'init': 'epsg:3395'}

def get_buffers(flows, point_column, cell_id=['i_start', 'j_start', 'i_end', 'j_end']):
    points = gpd.GeoDataFrame(flows[cell_id], geometry=flows[point_column], crs=lat_lon_projection)
    points_in_meters = points.to_crs(meters_projection)
    points_in_meters['geometry'] = points_in_meters['geometry'].apply(lambda p: p.buffer(RADIUS))
    return points_in_meters.to_crs(lat_lon_projection)
